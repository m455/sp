server {
    root /var/www/paste;
    server_name paste.example.com;
    location / {
        try_files $uri $uri/ =404;
    }
}
