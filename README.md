# sp

Scheme paste, a paste tool written in Chicken Scheme.

Also, a rewrite of my [fpaste project](https://git.m455.casa/fpaste).

## Contents

<!-- vim-markdown-toc GFM -->

* [Requirements](#requirements)
* [Installation](#installation)
    * [Installing sp](#installing-sp)
    * [Installing sp into a custom directory](#installing-sp-into-a-custom-directory)
* [Configuration](#configuration)
    * [Configuring nginx](#configuring-nginx)
    * [Configuring sp](#configuring-sp)
* [Usage](#usage)
* [Compiling a static binary from source](#compiling-a-static-binary-from-source)

<!-- vim-markdown-toc -->

## Requirements

- 64-bit Linux system
- A web server
- `rsync`
- `xclip`

## Installation

This Git repository comes with static binaries in the `builds` directory. By
default, sp installs into `~/.local/bin`.

If you want to install sp into a custom directory, refer to the [Installing sp
into a custom directory](#installing-sp-into-a-custom-directory) section.

### Installing sp

1. (Optional) Run `sha256sum` on a static binary in the `builds` directory to
   make sure the checksum of the static binary matches the value in the checksum
   file associated with the static binary you choose to use.
2. Run `make install`.

**Note**: If you don't have the `~/.local/bin` directory in your `$PATH`, then
add the following line to `~/.profile`:

```
export PATH="$PATH:$HOME/.local/bin"
```

This allows you to run the `sp` command from any directory on your system.

### Installing sp into a custom directory

You can change the directory that sp installs into by defining a value for the
`DIR_INSTALL` make variable.

For example, to install sp into your `~/bin`, follow the steps below:

1. Make sure there is a static binary in the `builds` directory.
2. Run `make install DIR_INSTALL=~/bin`.

## Configuration

sp uses [rsync](https://rsync.samba.org/) to synchronize files to a remote or
local directory that you can access over the web.

This section teaches you how to configure nginx to serve the synchronized files,
and how to change sp's configuration file so your files upload to your web
directory.

- **Note 1**: In the steps below, replace `example.com` with your domain name.
- **Note 2**: The examples below use systemd to restart nginx. If you use a
  different init system, then replace the `systemctl ...` commands with your
  init commands.

### Configuring nginx

As root, follow the steps below:

1. Run `mkdir -p /var/www/paste`.
2. Run `chown -R your_username:your_username /var/www/paste`.
3. Run `chmod -R 755 /var/www/paste`.
4. Add the contents of the `assets/paste.example.com` file to `/etc/nginx/sites-available/paste.example.com`.
5. Run `ln -s /etc/nginx/sites-available/paste.example.com /etc/nginx/sites-enabled/`.
6. Run `nginx -t` to check your nginx configuration for errors.
7. Run `systemctl restart nginx`.

### Configuring sp

You can find information about configuring sp in the configuration file at
`~/.sp`. If this file doesn't exist, then run `sp init`.

## Usage

- `sp help` - Displays this help message.
- `sp init` - Creates a config file at `~/.sp`
- `sp <path/to/file>` - Uploads a file to your remote paste directory. You can change your remote paste directory by editing your `~/.sp` configuration file.
- `sp rm <path/to/file>` - Deletes a file from your remote paste directory, where `<path/to/file>` should be a file in the `local-paste-directory` that is specified in your `~/.sp` configuration file.
- `sp sync`
    - **WARNING**: This command deletes files in your remote paste directory if they don't exist in your local paste directory.
    - Synchronizes the files in your local paste directory with the files in your remote paste directory.

## Compiling a static binary from source

A static binary is available in the `builds` directory. By default, this is
where the Makefile will create static binaries.

To compile a static binary from source, follow the steps below:

1. Make sure you have Chicken Scheme 5 and `gcc` installed.
2. Run `make`

