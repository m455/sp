src = src/main.scm
dir_install ?= ~/.local/bin
dir_build = builds
prog = sp
kernel = $(shell uname -s | tr 'A-Z' 'a-z')
hardware = $(shell uname -m)
bin = $(prog)-$(kernel)-$(hardware)

all: compile checksum

checksum: compile
	sha256sum ./$(dir_build)/$(bin) | cut -d ' ' -f1 > ./$(dir_build)/$(bin)-sha256.txt

compile:
	csc -O3 -static ./$(src) -o ./$(dir_build)/$(bin)
	rm ./$(dir_build)/$(bin).link

install:
	install -Dm755 ./$(dir_build)/$(bin) -D $(dir_install)/$(prog)

uninstall:
	rm $(dir_install)/$(prog)

clean:
	rm ./$(dir_build)/$(bin)

