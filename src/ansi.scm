(define escape-code "\033[")

(define normal 0)
(define bold 1)

(define colors
  '((black 30)
    (red 31)
    (green 32)
    (yellow 33)
    (blue 34)
    (magenta 35)
    (cyan 36)
    (white 37)
    (default 39)))

(define (colors-ref key)
  (get colors key))

(define (escape x)
  (let ((y (if (pair? x)
             (string-intersperse (map number->string x) ";")
             (number->string x))))
    (string-append escape-code y "m")))

(define (paint str color-key)
  (string-append (escape (list bold (colors-ref color-key)))
                 str
                 (escape (list normal (colors-ref 'default)))))
