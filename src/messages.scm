(define messages '((argument-not-provided "oh shit: argument not provided.")
                   (file-already-exists "oh shit: ~a already exists.")
                   (file-doesnt-exist "oh shit: ~a doesn't exist.")
                   (file-creating "creating ~a...")
                   (file-uploading "uploading ~a...")
                   (file-uploaded "uploaded ~a.")
                   (file-deleting-both "deleting local and remote copies of ~a...")
                   (file-deleted-both "deleted local and remote copies of ~a.")
                   (file-creating-finished "finished creating ~a!")
                   (file-not-in-local-paste-directory "oh shit: ~a isn't a file in ~a.")
                   (init-prompt "~a will be created. Is this okay? [y/n]")
                   (init-cancelled "cancelled initialization.")
                   (try-running "try running ~a.")
                   (for-help "for help, try running ~a.")
                   (synced "finished synchronizing ~a with ~a.")
                   (copied-to-clipboard "copied ~a to your clipboard.")))

