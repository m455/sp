(import utf8
        (chicken file)
        (chicken file posix)
        (chicken format)
        (chicken io)
        (chicken pathname)
        (chicken process)
        (chicken process-context)
        (chicken string))

(include "src/config.scm")
(include "src/ansi.scm")
(include "src/messages.scm")

(define config-file (make-pathname (get-environment-variable "HOME") ".sp"))

(define (displayln str)
  (display str)
  (newline))

(define-syntax displayln-format
  (syntax-rules ()
    ((_ str v ...)
     (displayln (format str v ...)))))

;; use pair? here, instead of list?, because list? returns #t if alist is
;; '()
(define (get alist key)
  (if (and (pair? alist)
           (pair? (car alist))
           (symbol? key))
    (cadr (assq key alist))
    alist))

(define (keys alist)
  (map car alist))

(define (vals alist)
  (map cadr alist))

(define (file->sexp f)
  (if (file-exists? f)
    (with-input-from-file f (lambda () (read)))
    '()))

(define (expand-path-home path)
  (if (equal? "~" (substring path 0 1))
    (make-pathname (get-environment-variable "HOME") (substring path 1))
    path))

(define (has-trailing-slash? str)
  (eq? #\/ (string-ref str (- (string-length str) 1))))

(define (config-fix-value str)
  (if (has-trailing-slash? str)
    str
    (string-append str "/")))

(define config-alist (if (file-exists? config-file)
                       (file->sexp config-file)
                       '()))

(define (config-get key)
  (case key
    ('url (config-fix-value (get config-alist key)))
    ((remote-paste-directory local-paste-directory)
     (expand-path-home (config-fix-value (get config-alist key))))
    (else (get config-alist key))))

;; this seems unnecessary, but i was using (get messages ...) so often that i
;; just needed this
(define (message key)
  (get messages key))

;; TODO maybe remove this?
(define (run-shell-command str)
  (define-values (in out process-id) (process str)))

(define (conditional-paint str color-key)
  (if (equal? (config-get 'color) "yes")
    (paint str color-key)
    str))

(define (displayln-to-file path contents)
  (if (file-exists? path)
    (displayln-format (message 'file-already-exists)
                      (conditional-paint path 'red))
    (with-output-to-file path (lambda () (displayln contents)))))

(define (string-lowercase s)
  (string-translate s "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                      "abcdefghijklmnopqrstuvwxyz"))

(define (init-prompt)
  (displayln-format (message 'init-prompt)
                    (conditional-paint config-file 'blue))
  (display "> ")
  (let ((input (string-lowercase (read-line))))
    (if (equal? "y" input)
      (begin (displayln-format (message 'file-creating)
                               (conditional-paint config-file 'blue))
             (displayln-to-file config-file config-file-contents)
             (displayln-format (message 'file-creating-finished)
                               (conditional-paint config-file 'blue)))
      (displayln (message 'init-cancelled)))))

(define (file-fix-extension path)
  (let* ((file (pathname-strip-directory path))
         (file-extension (pathname-extension file)))
    (if (member file-extension (config-get 'preserved-extensions))
      file
      (string-append file ".txt"))))

(define (path->paste-url path)
  (let ((url (config-get 'url))
        (file (file-fix-extension path)))
    (if (member (pathname-extension file) (config-get 'preserved-extensions))
      (string-append url file)
      (string-append url file ".txt"))))

(define (file-backup path)
  (let* ((file (file-fix-extension path))
         (paste-directory (config-get 'local-paste-directory))
         (destination (make-pathname paste-directory file)))
    ;; #t here overwrites the file if it exists
    (copy-file path destination #t)
    ;; 420 is 644 permissions (-rw-r--r--)
    (set-file-permissions! destination 420)))

(define (file-upload path)
  (let* ((file (file-fix-extension path))
         (source (make-pathname (config-get 'local-paste-directory) file))
         (destination (config-get 'remote-paste-directory)))
    (call-with-input-pipe
      (format "rsync -a --delete ~a ~a" source destination) read)))

(define (copy-to-clipboard path)
  (let ((paste-url (path->paste-url path)))
    (system (format "echo \"~a\" | tr -d '\n' | xclip -selection clipboard" paste-url))
    (displayln-format (message 'copied-to-clipboard)
                      (conditional-paint paste-url 'green))))

(define (paste-file arg)
  (let ((path (expand-path-home arg)))
    (if (file-exists? config-file)
      (if (file-exists? path)
        (begin (displayln-format (message 'file-uploading)
                                 (conditional-paint path 'yellow))
               (file-backup path)
               (file-upload path)
               (displayln-format (message 'file-uploaded)
                                 (conditional-paint path 'green))
               (copy-to-clipboard path))
        (displayln-format (message 'file-doesnt-exist)
                          (conditional-paint path 'red)))
      (begin (displayln-format (message 'file-doesnt-exist)
                               (conditional-paint config-file 'red))
             (displayln-format (message 'try-running)
                               (conditional-paint "sp init" 'green))))))

(define (sync verbose?)
  (if (file-exists? config-file)
    (let* ((local-paste-directory (config-get 'local-paste-directory))
           (remote-paste-directory (config-get 'remote-paste-directory)))
      (call-with-input-pipe
        (format "rsync -a --delete ~a ~a" local-paste-directory remote-paste-directory) read-line)
      (when verbose?
        (displayln-format (message 'synced)
                          (conditional-paint local-paste-directory 'blue)
                          (conditional-paint remote-paste-directory 'blue))))
    (begin (displayln-format (message 'file-doesnt-exist)
                             (conditional-paint config-file 'red))
           (displayln-format (message 'try-running)
                             (conditional-paint "sp init" 'green)))))

(define (rm file)
  (let* ((path (expand-path-home file))
         (local-paste-directory (config-get 'local-paste-directory))
         ;; so we don't error out on dotfiles
         ;; for example, (pathname-extension ".profile") returns false
         (file-extension (let ((ext (pathname-extension path)))
                           (if ext
                             (string-append "." ext)
                             ""))))
    (if (file-exists? path)
      (let ((path-file (string-append (pathname-file path) file-extension)))
        (if (or (equal? (string-append local-paste-directory path-file) path)
                (equal? (string-append local-paste-directory path-file)
                        (expand-path-home (string-append "~/" path))))
          (begin (displayln-format (message 'file-deleting-both)
                                   (conditional-paint path-file 'yellow))
                 (delete-file* path)
                 (sync #f)
                 (displayln-format (message 'file-deleted-both)
                                   (conditional-paint path-file 'green)))
          (displayln-format (message 'file-not-in-local-paste-directory)
                            (conditional-paint path 'red)
                            (conditional-paint local-paste-directory 'blue))))
      (displayln-format (message 'file-doesnt-exist)
                        (conditional-paint file 'red)))))

(define (help)
  (displayln
    #<<STRING
sp - A tool for sharing uploaded files with people.

Usage:
  sp help - Display this help message.
  sp init - Create a config file at '~/.sp'.
  sp <path/to/file> - Upload a file to your remote paste directory. You can change your remote paste directory by editing your '~/.sp' configuration file.
  sp rm <path/to/file> - Deletes a file from your remote paste directory, where '<path/to/file>' should be a file in the 'local-paste-directory' that is specified in your '~/.sp' configuration file.
  sp sync - WARNING: This command deletes files in your remote paste directory if they don't exist in your local paste directory. Synchronize the files in your local paste directory with the files in your remote paste directory.
STRING
))

(define (init)
  (if (file-exists? config-file)
    (displayln-format (message 'file-already-exists)
                      (conditional-paint config-file 'red))
    (init-prompt)))

(define (main args)
  (if (null? args)
    (displayln-format (message 'for-help)
                      (conditional-paint "sp help" 'green))
    (let ((first-arg (car args))
          (second-arg (if (> (length args) 1)
                        (cadr args)
                        #f)))
      (case (string->symbol first-arg)
        ('init (init))
        ('sync (sync #t))
        ('rm (if second-arg
               (rm second-arg)
               (begin (displayln-format (message 'argument-not-provided))
                      (displayln-format (message 'try-running)
                                        (conditional-paint "sp rm <path/to/file>" 'green)))))
        ((--help -help help -h) (help))
        (else (paste-file first-arg))))))

(main (command-line-arguments))

